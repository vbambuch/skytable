export default {
    searchCity: 'search-city',
    searchFlight: 'search-flight',
    printFlights: 'print-flights',
    clearCities: 'clear-cities',
    printCities: 'print-src-cities',
    printDstCities: 'print-dst-cities',
    citySrcSelected: 'city-src-selected',
    cityDstSelected: 'city-dst-selected',
    dateSelected: 'date-selected',
    flightsError: 'flight-error',
    flightsOk: 'flight-ok',
    flightsEmpty: 'flight-empty',
    loading: 'loading'
};