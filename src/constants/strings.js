export default {
    somethingWrong: 'Nastala neočekávaná chyba.',
    noFlights: 'Nebyly nalezeny žádné lety.',
    wrongForm: 'Pro vyhledání letů prosím vyplňte všechny položky formuláře.'
};