import dispatcher from '../dispatcher';
import actions from '../constants/actions';
import strings from '../constants/strings';
import { EventEmitter } from 'events';
import $ from 'jquery';

class MainStore extends EventEmitter{


}

class Search {
    constructor() {
        this.src = undefined;
        this.dst = undefined;
        this.when = '';
        this.back = '';
    }

    checkOptions() {
        return (this.src != undefined && this.dst != undefined && this.when != '');
    }

}

const mainStore = new MainStore();
const search = new Search();

dispatcher.register(function(payload) {
    switch (payload.type) {
        case actions.searchCity:
            let name = payload.action;
            let length = name.length;
            let method = actions.printCities;

            if (length > 1) {
                $.ajax({
                    url: 'https://api.skypicker.com/places',
                    type: 'GET',
                    data: {term: name, v: 2},
                    success: function (response) {
                        mainStore.emit(method, response);
                    },
                    error: function () {
                        mainStore.emit(actions.flightsError, strings.somethingWrong);
                    }
                });
            }
            else if (payload.form.toString() === 'src') {
                search.src = undefined;
            }
            else if (payload.form.toString() === 'dst') {
                search.dst = undefined;
            }
            break;

        case actions.searchFlight:
            if (search.checkOptions()) {
                mainStore.emit(actions.loading);
                let options = {
                    v: 2,
                    locale: 'cz',
                    
                    flyFrom: search.src.id,
                    to: search.dst.id,
                    dateFrom: search.when,
                    dateTo: search.when,

                    typeFlight: 'oneway',
                    returnFrom: search.back,
                    returnTo: search.back
                };
                
                $.ajax({
                    url: 'https://api.skypicker.com/flights',
                    type: 'GET',
                    data: options,
                    success: function (response) {
                        mainStore.emit(actions.printFlights, response);
                    },
                    error: function () {
                        mainStore.emit(actions.flightsError, strings.somethingWrong);
                    }
                });
            }
            else {
                mainStore.emit(actions.flightsError, strings.wrongForm);
            }
            break;

        case actions.flightsEmpty:
            mainStore.emit(actions.flightsError, strings.noFlights);
            break;

        case actions.flightsOk:
            mainStore.emit(actions.flightsOk);
            break;
        
        case actions.citySrcSelected:
            search.src = payload.action;
            break;

        case actions.cityDstSelected:
            search.dst = payload.action;
            break;
        
        case actions.dateSelected:
            search.when = payload.action;
            break;
    }
}.bind(this));

export default mainStore;