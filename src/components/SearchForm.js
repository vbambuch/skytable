import React from 'react';
import dispatcher from '../dispatcher';
import MainStore from '../stores/mainStore'
import actions from '../constants/actions';
import DatePicker from 'material-ui/DatePicker';
import Text from 'material-ui/AutoComplete';
import RaisedButton from 'material-ui/RaisedButton';
import DateFormat from 'dateformat';
import {Card} from 'material-ui/Card';

const pickerStyle = {
    // backgroundColor: '#CDDC39'
};

export default class SearchForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            valueSrc: '',
            valueDst: '',
            cities: [],
            names: []
        }
    }

    componentDidMount() {
        MainStore.on(actions.printCities, function (cities) {
            this.state.cities = cities;
            const array = [];
            _.forEach(cities, function(value, key) {
                array.push(value.value);
            });
            this.setState({names: array});
        }.bind(this));
    }

    render() {
        return (
            <Card>
                <form onSubmit={this.handleSearch}>
                    <div className="searchForm">
                        <div className="inputText">
                            <Text
                                id="from"
                                type="text"
                                placeholder="Odkud"
                                ref="inputSrc"
                                onUpdateInput={this.handleSrcCityChange}
                                onNewRequest={this.handleSrcCitySelected.bind(this)}
                                value={this.state.valueSrc}
                                dataSource={this.state.names}/>
                        </div>
                        <div className="inputText">
                            <Text
                                id="to"
                                type="text"
                                placeholder="Kam"
                                ref="inputDst"
                                onUpdateInput={this.handleDstCityChange}
                                onNewRequest={this.handleDstCitySelected.bind(this)}
                                value={this.state.valueDst}
                                dataSource={this.state.names}/>
                        </div>
                        <div className="datePicker">
                            <DatePicker
                                container="inline"
                                formatDate={this.handleDateFormat}
                                type="text"
                                placeholder="Datum"
                                ref="inputWhen"
                                id="date"
                                style={pickerStyle}
                                mode="landscape"
                                onChange={this.handleDateChange}/>
                        </div>
                    </div>
                    <RaisedButton
                        label="Hledat"
                        onMouseDown={this.handleSearch}
                        className="searchButton"/>
                </form>
            </Card>
        );
    }

    handleSrcCityChange(value) {
        dispatcher.dispatch({
            type: actions.searchCity,
            action: value,
            form: 'src'
        });
    }

    handleDstCityChange(value) {
        dispatcher.dispatch({
            type: actions.searchCity,
            action: value,
            form: 'dst'
        });
    }

    handleSrcCitySelected(value, index) {
        let city = this.state.cities[index];
        dispatcher.dispatch({
            type: actions.citySrcSelected,
            action: city
        });
    }
    
    handleDstCitySelected(value, index) {
        let city = this.state.cities[index];
        dispatcher.dispatch({
            type: actions.cityDstSelected,
            action: city
        });
    }

    handleDateChange(event, date) {
        dispatcher.dispatch({
            type: actions.dateSelected,
            action: DateFormat(date, "dd/mm/yyyy")
        });
    }

    handleSearch(event) {
        event.preventDefault();
        dispatcher.dispatch({type: actions.searchFlight});
    }

    handleDateFormat(date) {
        return DateFormat(date, "dd/mm/yyyy");
    }
}