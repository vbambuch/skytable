import React from 'react';
import dispatcher from '../../dispatcher';
import { constants } from '../../constants/actions';


export default class City extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            city: {}
        }
    }

    render() {
        return (
            <tr
                className="cityHelper"
                onClick={this.handleClick.bind(this)}>{this.props.value}
            </tr>
        );
    }

    handleClick(event) {
        let method = constants.citySrcSelected;
        if (this.props.method === 'dst') {
            method = constants.cityDstSelected;
        }
        dispatcher.dispatch({type: method, action: this.props});
    }
}