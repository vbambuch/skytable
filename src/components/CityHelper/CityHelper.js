import React from 'react';
import City from './City';
import MainStore from '../../stores/mainStore'
import { constants } from '../../constants/actions';


export default class CityHelper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            method: ''
        }
    }

    componentDidMount() {
        // MainStore.on(actions.printCities, function (cities) {
        //     this.setState({
        //         cities: cities,
        //         method: 'src'
        //     });
        // }.bind(this));
        //
        // MainStore.on(actions.printDstCities, function (cities) {
        //     this.setState({
        //         cities: cities,
        //         method: 'dst'
        //     });
        // }.bind(this));
        //
        // MainStore.on(actions.clearCities, function () {
        //     this.setState({ cities: []});
        // }.bind(this));
    }

    render() {
        return (
            <table>
                <tbody>
                    {this.renderCities()}
                </tbody>
            </table>
        );
    }
    renderCities() {
        return _.map(this.state.cities, (city, index) =>
            <City
                key={index}
                method={this.state.method}
                {...city}
            />);
    }
}