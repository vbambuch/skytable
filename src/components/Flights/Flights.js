import _ from 'lodash';
import React from 'react';
import FlightItem from './FlightItem';
import dispatcher from '../../dispatcher';
import MainStore from '../../stores/mainStore'
import actions from '../../constants/actions';
import {Card} from 'material-ui/Card';

export default class Flights extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flights: []
        }
    }

    componentDidMount() {
        MainStore.on(actions.printFlights, function (flights) {
            if (!flights.data.length > 0) {
                this.handleEmptyData();
                return;
            }
            this.handleCorrectData();
            this.setState({flights});
        }.bind(this));

        MainStore.on(actions.flightsError, function () {
            this.setState({flights: []});
        }.bind(this));

        MainStore.on(actions.loading, function () {
            this.setState({flights: []});
        }.bind(this));
    }
    
    render() {
        return (
            <div>
                <Card>
                    {this.renderFlights()}
                </Card>
            </div>
        );
    }

    renderFlights() {
        return _.map(this.state.flights.data, (flight, index) =>
            <FlightItem key={index} {...flight}/>);
    }
    
    handleEmptyData() {
        dispatcher.dispatch({
            type: actions.flightsEmpty,
            action: actions.noFlights
        });
    }
    
    handleCorrectData() {
        dispatcher.dispatch({
            type: actions.flightsOk
        });
    }
}