import React from 'react';
import {Card} from 'material-ui/Card';

export default class FlightItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render() {
        return (
            <Card className="flightItem">
                <table>
                    <tbody>
                        <tr>
                            <td rowSpan="3" className="flightPrice">
                                {this.props.price} €
                            </td>
                            <td colSpan="2" className="flightInfo">
                                {this.props.cityFrom}
                                {" -- "}
                                {this.props.cityTo}
                            </td>
                        </tr>
                        <tr>
                            <td>{this.getDate(this.props.aTimeUTC)}</td>
                            <td>{this.getTime(this.props.aTimeUTC)}</td>
                        </tr>
                        <tr>
                        </tr>
                    </tbody>
                </table>
            </Card>
        );
    }

    getDate(number) {
        let date = new Date(number * 1000);
        let month = date.getUTCMonth();
        month++;

        let ret = date.getUTCDate() + '.' +
            month + '.' +
            date.getUTCFullYear();
        return ret;
    }

    getTime(number) {
        let date = new Date(number * 1000);
        let hour = date.getUTCHours();
        let minutes = date.getUTCMinutes();

        if (hour.toString().length < 2) {
            hour = "0" + hour;
        }
        if (minutes.toString().length < 2) {
            minutes = "0" + minutes;
        }
        return hour + ':' + minutes;
    }
}