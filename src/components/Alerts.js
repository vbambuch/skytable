import React from 'react';
import MainStore from '../stores/mainStore'
import actions from '../constants/actions';
import strings from '../constants/strings';
import {Card, CardText} from 'material-ui/Card';
import {red400} from 'material-ui/styles/colors'
import RefreshIndicator from 'material-ui/RefreshIndicator';

const cardTextStyle = {
    textAlign: 'center',
    color: red400,
    display: 'block'
};

const loadingStyle = {
    display: 'none',
    position: 'relative'
};

export default class Alerts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: strings.wrongForm,
            cardStyle: {
                display: 'block'
            },
            cardTextStyle,
            loadingStyle
        };
    }

    componentDidMount() {
        MainStore.on(actions.flightsError, function (text) {
            this.setState({text, cardStyle: {display: 'block'}});
        }.bind(this));

        MainStore.on(actions.printFlights, function () {
            let style = loadingStyle;
            style.display = 'none';
            this.setState({loadingsStyle: style});
        }.bind(this));

        MainStore.on(actions.flightsError, function () {
            let style = loadingStyle;
            style.display = 'none';
            this.setState({loadingsStyle: style});
        }.bind(this));

        MainStore.on(actions.loading, function () {
            let style = loadingStyle;
            style.display = 'inline-block';
            this.setState({
                loadingsStyle: style,
                cardStyle: {display: 'none'}
            });
        }.bind(this));
    }
    
    render() {
        return (
            <div>
                <Card
                    style={this.state.cardStyle}>
                    <CardText
                        style={cardTextStyle}>
                        <h2>{this.state.text}</h2>
                    </CardText>
                </Card>
                <div className="loadingBox">
                    <RefreshIndicator
                        size={80}
                        left={0}
                        top={0}
                        status="loading"
                        style={loadingStyle}/>                    
                </div>
            </div>
        );
    }
}