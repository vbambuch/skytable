import React from 'react';
import Flights from './Flights/Flights';
import SearchForm from './SearchForm';
import Alerts from './Alerts';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Card} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

const buttonStyle = {
    padding: '5px 30px 5px 30px',
    height: '50px'
};

const labelStyle = {
    fontSize: 'large'
};

const titleStyle = {
    fontFamily: 'Roboto, sans-serif',
    textAlign: 'center',
    fontSize: 50,
    marginTop: 0,
    marginBottom: 15,
    paddingTop: 20,
    color: 'white'
};

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#CDDC39',
        primary2Color: '#CDDC39',
        primary3Color: '#CDDC39'
    }
});

export default class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    <div className="backColor">
                        <div className="center searchBlock">
                            <h1 style={titleStyle}>Skytable</h1>
                            <Card className="menu">
                                <FlatButton
                                    label="Letenky"
                                    style={buttonStyle}
                                    labelStyle={labelStyle}/>
                                <FlatButton
                                    label="Ubytování"
                                    style={buttonStyle}
                                    labelStyle={labelStyle}/>
                                <FlatButton
                                    label="Parking"
                                    style={buttonStyle}
                                    labelStyle={labelStyle}/>
                            </Card>
                            <SearchForm />
                        </div>
                    </div>
                    <div className="center flights">
                        <Flights/>
                    </div>
                    <div className="center">
                        <Alerts/>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}